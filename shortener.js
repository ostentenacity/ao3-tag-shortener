// ==UserScript==
// @name         AO3 Canonical Tag Shortener
// @namespace    https://bitbucket.org/ostentenacity/
// @version      0.1
// @description  Make long AO3 canonical tags more readable
// @author       Ostentenacity
// @match        http://archiveofourown.org/*
// @match        https://archiveofourown.org/*
// @grant        none
// ==/UserScript==

/*Customize tags to be replaced by editing the userscript and adding them like so in the indicated spot:
'Annoyingly Long Canonical Tag I Want To Replace': 'Shorter Version',
'A Second Annoyingly Long Canonical Tag': 'A Second Replacement'
Make sure to use TWO PAIRS OF QUOTE MARKS and INCLUDE THE COLON IN THE MIDDLE!
All tag pairs should be on separate lines, and all but the last tag pair should have a comma at the end AFTER THE LAST QUOTE MARK.
(If this list of tag pairs gets messed up even a little bit it will break the userscript until you fix it, so double-check your formatting.)
If your canonical or shortened tag contains a single quote mark/apostrophe, put a backslash (\) before the single quote/apostrophe wherever it appears.
Don't worry, it will look normal on AO3 even if you use a backslash in the shortened tag.
Example: `'Jonathan "Jon" Sims | The Archivist\'s Grandmother' : 'Jonathan Sims\'s Grandmother'`*/

(function() {
    'use strict';

var substitutions = {
    // PUT CUSTOM TAGS BELOW THIS LINE
    'Jonathan "Jon" Sims | The Archivist': 'Jonathan Sims'
    // PUT CUSTOM TAGS ABOVE THIS LINE
}

// replace tags
var tags = document.getElementsByClassName('tag');
for (var tag of tags) {
    for (var key in substitutions) {
        tag.text = tag.text.replace(key, substitutions[key]);
    }
}

// replace filters
var filters = [
    document.getElementById('include_character_tags').getElementsByTagName('span'),
    document.getElementById('exclude_character_tags').getElementsByTagName('span'),
    document.getElementById('include_relationship_tags').getElementsByTagName('span'),
    document.getElementById('exclude_relationship_tags').getElementsByTagName('span'),
    document.getElementById('include_freeform_tags').getElementsByTagName('span'),
    document.getElementById('exclude_freeform_tags').getElementsByTagName('span')
];

for (var filter of filters) {
    for (var desc of filter) {
        if (desc.textContent) {
            for (key in substitutions) {
                desc.textContent = desc.textContent.replace(key, substitutions[key]);
            }
        }
    }
}
})();