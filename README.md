# README #

This is a userscript to change specific AO3 canonical tags to user-created versions for readability. It is an unofficial userscript not associated with the OTW. It may cause pages to load slightly slower, especially if you add a lot of custom tags.

### Setup instructions ###

1. If you don't already have a userscript manager, install one. I suggest [Tampermonkey](https://www.tampermonkey.net/), which is available as a browser extension for most browsers.
2. Open the [source code file](https://bitbucket.org/ostentenacity/ao3-tag-shortener/src/master/shortener.js) for this userscript. Click the three little dots on the right-hand side of the gray title bar and select the option for "Open Raw" to view as plain code.
3. Select and copy all the text on the page.
4. In your userscript manager, create a new script. (With the Tampermonkey extension, just click the icon in your toolbar and select "Create a new script" in the dropdown.)
5. Paste and save the source code.
6. If your script is not enabled by default, enable it manually. (For Tampermonkey, ensure that the switch in the dashboard is green/in the right-hand position.)
7. Done!
8. If you run into any problems, contact [Ostentenacity](https://ostentenacity.tumblr.com).

### Customization instructions ###

Customize tags to be replaced by editing the userscript and adding them in the indicated spot using the following format:
```
'Annoyingly Long Canonical Tag I Want To Replace': 'Shorter Version',
'A Second Annoyingly Long Canonical Tag': 'A Second Replacement'
```
Make sure to use **two pairs of quotes** and **include the colon in the middle!** All tag pairs should be on separate lines, and all but the last tag pair should have a comma at the end **after the last quote mark.**

(If this list of tag pairs gets messed up even a little bit it will break the userscript until you fix it, so double-check your formatting. Computers are so much fun.)

If your canonical or shortened tag contains a single quote mark/apostrophe, put a backslash (\\) before the single quote/apostrophe wherever it appears. Don't worry, it will look normal on AO3 even if you use a backslash in the shortened tag.

Example: `'Jonathan "Jon" Sims | The Archivist\'s Grandmother' : 'Jonathan Sims\'s Grandmother'`

Any text you put in as a tag to be replaced will appear in every place where it shows up in any tag, character filter, relationship filter, or freeform tag filter. This means that if it's a character tag that's bothering you, all the relationship or freeform tags that use the long version of the tag will be fixed (i.e. even if you shorten only the character tag, 'Jonathan "Jon" Sims | The Archivist/Martin Blackwood' will be corrected to 'Jonathan Sims/Martin Blackwood' without needing to explicitly shorten the ship tag), but it also means that it will do this every single time this script finds that exact text in a tag or filter. (So, for example, if you changed "ass" to "butt", any tag with the word "associated" would have that word changed to "buttociated". Use with caution and/or impunity.)
